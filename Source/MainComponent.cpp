/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    audioDeviceManager.initialiseWithDefaultDevices(2, 2);
    audioDeviceManager.addAudioCallback (this);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback(this);
}

void MainComponent::resized()
{

}

void MainComponent::audioDeviceAboutToStart(AudioIODevice *device)
{
    DBG ("audioDeviceAboutToStart");
    
}

void MainComponent::audioDeviceStopped()
{
    DBG ("audioDeviceStopped");
    
}

void MainComponent::audioDeviceIOCallback (const float** inputChannelData,
                                            int numInputChannels,
                                            float** outputChannelData,
                                            int numOutputChannels,
                                            int numSamples)
{
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        *outL = *inL;
        *outR = *inR;
        
        inL++;
        outL++;
        outR++;
    }
}
